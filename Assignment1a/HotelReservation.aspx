﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelReservation.aspx.cs" Inherits="Assignment1a.HotelReservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Booking System</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%--how to align label text to centre in asp reffered from 
                https://stackoverflow.com/questions/13797492/how-to-align-text-to-center-in-asp-label  --%>

            <div style="margin-left: auto; margin-right: auto; text-align: center;">
             <asp:Label ID="Label3" runat="server" Text="Hotel Booking Zone" Font-Bold="true" Font-Size="X-Large" CssClass="StrongText"></asp:Label>
            </div>
            <br />

            <asp:Label runat="server">Name: </asp:Label><asp:TextBox runat="server" id="name" placeholder="e.g. John doe"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter name." ForeColor="Red" ControlToValidate="name" ID="validateName"></asp:RequiredFieldValidator>
            <br />
            <br />

            <asp:Label runat="server">Address: </asp:Label><asp:TextBox runat="server" ID="address" placeholder="e.g. 123, Canada"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter address" ForeColor="Red" ControlToValidate="address" ID="validateAddress"></asp:RequiredFieldValidator>
            <br />
            <br />

            <asp:Label runat="server">Email: </asp:Label><asp:TextBox runat="server" ID="email" placeholder="e.g. johndoe@example.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ForeColor="Red" ControlToValidate="email" ID="validateEmailAddress"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="validateEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email" ForeColor="Red" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Label runat="server">Contact number: </asp:Label><asp:TextBox runat="server" ID="contactNumber" placeholder="10-12 digit contact number"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ForeColor="Red" ControlToValidate="contactNumber" ID="validatorPhone" Display="Dynamic"></asp:RequiredFieldValidator>
           
            <%-- to validate pone number reffered to an article "Validate Mobile Number With 10 Digits In ASP.NET"
                By Prashant Dubey
                https://www.c-sharpcorner.com/blogs/validate-mobile-number-with-10-digits-in-asp-net --%>

            <asp:RegularExpressionValidator ID="RegularExpressionValidatorcontactnumber" runat="server" ControlToValidate="contactNumber" ForeColor="Red" ErrorMessage="Enter a valid Contact Number" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>          
            <br />
            <br />

            <asp:Label runat="server">ID Proof: </asp:Label><asp:TextBox runat="server" ID="idProof" placeholder="XXXXXXXXXXX"></asp:TextBox>
            <asp:RequiredFieldValidator ID="validateIDProof" runat="server" ErrorMessage="Please enter ID Proof number" ForeColor="Red" ControlToValidate="idProof"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Label runat="server">Room Type</asp:Label>
            <asp:DropDownList runat="server" ID="roomType">
                <asp:ListItem Text="Deluxe Room" Value="D"></asp:ListItem>
                <asp:ListItem Text="Super Deluxe Room" Value="SD"></asp:ListItem>
                <asp:ListItem Text="Tripple Bedded Room" Value="TBR"></asp:ListItem>
                <asp:ListItem Text="Family Suite" Value="FS"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Label runat="server">Number Of Guests</asp:Label>
            <asp:DropDownList runat="server" ID="numberOfGuests">
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                <asp:ListItem Text="9" Value="9"></asp:ListItem>
            </asp:DropDownList>
            <br />          
            <br />
            <%--
                Regular Expression for date format is reffered from https://www.regextester.com/96683
                and CompareValidator for date is reffered from stackoverflow By Ebad Masood
                https://stackoverflow.com/questions/9372901/asp-net-compare-validator-to-validate-date
                --%>
            <asp:Label runat="server">Check-In Date: </asp:Label><asp:TextBox runat="server" ID="checkinate" placeholder="yyyy-mm-dd"></asp:TextBox>
            <asp:RequiredFieldValidator ID="validateRoomType" runat="server" ForeColor="Red" ErrorMessage="Check-In Date Required"  ControlToValidate="checkinate"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorcheckindate" runat="server" ValidationExpression="([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))" ControlToValidate="checkinate" ForeColor="Red" ErrorMessage="Invalid Date Format" Display="Dynamic"></asp:RegularExpressionValidator>
            
            <asp:Label runat="server">   Check-Out Date: </asp:Label><asp:TextBox runat="server" ID="checkoutate" placeholder="yyyy-mm-dd"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ErrorMessage="Check-Out Date Required" ControlToValidate="checkoutate" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorcheckoutdate" runat="server" ValidationExpression="([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))" ControlToValidate="checkoutate" ForeColor="Red" ErrorMessage="Invalid Date Format"></asp:RegularExpressionValidator>
            
            <asp:CompareValidator id="checkinoutDate" runat="server" 
             ControlToCompare="checkinate" cultureinvariantvalues="true" 
             display="Dynamic" enableclientscript="true"  
             ControlToValidate="checkoutate" 
             ErrorMessage="Check-IN date must be earlier than Check-Out date"
             type="Date" setfocusonerror="true" Operator="GreaterThanEqual" 
             text="Check-IN date must be earlier than Check-Out date"></asp:CompareValidator>
            <br />
            <br />
            <asp:Label runat="server">Pickup Service Needed?</asp:Label>
            <asp:RadioButton runat="server" Text="Yes" GroupName="pickupService" />
            <asp:RadioButton runat="server" Text="No" GroupName="pickupService" Checked="true" />
            <br />
            <br />
            <asp:Label runat="server">Meals: </asp:Label>
            <br />
            <br />
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:CheckBox ID="cbBreakfast" runat="server" Text="Breakfast" />
            <asp:CheckBox ID="cbLunch" runat="server" Text="Lunch" />
            <asp:CheckBox ID="cbDinner" runat="server" Text="Dinner" />
            <br />
            <br />
            <asp:Label runat="server">Add Ons: </asp:Label>
            <br />
            <br />
             &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:CheckBox ID="cbExtraMattress" runat="server" Text="Extra Mattress" />
            <asp:CheckBox ID="cbCharger" runat="server" Text="Charger" />
            <asp:CheckBox ID="cbDrinks" runat="server" Text="Drinks" />
            <br />
            <br />

            <asp:Label runat="server">Payment Mode</asp:Label>
            &nbsp&nbsp<asp:RadioButton runat="server" Text="Pay at Hotel" GroupName="paymentMode" />
            <asp:RadioButton runat="server" Text="Credit/Debit" GroupName="paymentMode" Checked="true" />

            <br />
            <br />
            <asp:Button runat="server" ID="myButton" Text="Submit Details"/>
            <br />
            <br />
            <%-- took reference for Validation summary from 
                Kudvenkat channel on youtube 
                https://www.youtube.com/watch?v=HI-koNrINTc   --%>
            <asp:ValidationSummary ID="validationSummary" runat="server" HeaderText="Validation summary" />
        </div>
    </form>
</body>
</html>
